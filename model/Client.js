'use strict';
const mongoose = require('mongoose');

const clientSchema = mongoose.Schema({
  gender: String,
  name: {
    first: String,
    last: String
  },
  location: {
    street: {
      number: String,
      name: String
    },
    city: String,
    country: String,
    postcode: Number,
  },
  email: String,
  phone: String,
  picture: {
    large: String,
    medium: String,
    thumbnail: String
  },
  login: {
    customerId: String
  }
});

module.exports = mongoose.model('Client', clientSchema);
