"use strict";
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  user: String,
  status: String,
  role: String,
  created: {type:Date, default:Date.now},
  password: String,
  lastin: Date,
  uuid: String,
  client: {type:mongoose.Schema.ObjectId, ref:'Client'}
});

module.exports = mongoose.model('User', userSchema);
