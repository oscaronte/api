'use strict';
const mongoose = require('mongoose');

const exchangeSchema = mongoose.Schema({
  idSerie: String,
  titulo: String,
  fecha: String,
  dato: String
});

module.exports = mongoose.model('ExchangeRate', exchangeSchema);
