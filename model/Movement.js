'use strict';
const mongoose = require('mongoose');

const movementSchema = new mongoose.Schema({
  account: {type:mongoose.Schema.ObjectId, ref:'Account'},
  description: String,
  date: {type:Date, default:Date.now},
  status: String,
  accounting: {
    amount: Number,
    currency: String,
    category: String
  },
  establishment: {
    id: String,
    name: String
  }
});

module.exports = mongoose.model('Movement', movementSchema);
