'use strict';
const mongoose = require('mongoose');

const accountSchema = mongoose.Schema({
  client: {type:mongoose.Schema.ObjectId, ref:'Client'},
  status: String,
  number: String,
  type: String,
  product: String,
  cutoffDay: Number,
  balance: Number,
});

module.exports = mongoose.model('Account', accountSchema);
