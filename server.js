"use strict";
const express = require('express'),
  app = express(),
  port = process.env.PORT || 3001;

const hostname = '127.0.0.1';
const requestjson = require('request-json');
const bodyparser = require('body-parser');
const mongoose = require('mongoose');
const utils = require('./modules/Utils');
const users_routes = require('./routes/user');
const clients_routes = require('./routes/client');
const accounts_routes = require('./routes/account');
const movements_routes = require('./routes/movement');
const market_routes = require('./routes/bbvaroutes');
const banxico_routes = require('./routes/banxicoroutes');

var ipMongo = process.env.DOCKER_HOST_IP || '127.0.0.1';

mongoose.connect('mongodb://apiusr:Accoun+s9@' + ipMongo + ':27017/apiaccounts', {useNewUrlParser: true, useUnifiedTopology: true});
app.use(bodyparser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header("Access-Control-Expose-Headers", "Refresh");
  next();
});

app.use('/api/Users', users_routes);
app.use('/api/Clients', clients_routes);
app.use('/api/Accounts', accounts_routes);
app.use('/api/Movements', movements_routes);
app.use('/api/BBVA', market_routes);
app.use('/api/Banxico', banxico_routes);

app.get('/', function(req, res) {
  utils.doResponse(res, 200, "Servicio corriendo");
});

app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
