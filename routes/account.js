'use strict';
const express = require('express');
const authservice = require('../services/Authentication');
const refreshservice = require('../services/RefreshAuth');
const controller = require('../controllers/account');

var api = express.Router();

api.use(authservice.validateToken);
api.use(refreshservice.refreshToken);

api.post('/', controller.create);

api.put('/:accountId?', controller.update);

api.delete('/:accountId?', controller.deleteItem);

api.patch('/', controller.off);

api.get('/:accountId?', controller.get);

module.exports = api;
