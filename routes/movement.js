'use strict';
const express = require('express');
const authservice = require('../services/Authentication');
const refreshservice = require('../services/RefreshAuth');
const controller = require('../controllers/movement');

var api = express.Router();

api.use(authservice.validateToken);
api.use(refreshservice.refreshToken);

api.post('/:accountId', controller.create);

api.put('/:accountId/:movementId?', controller.update);

api.delete('/:accountId/:movementId', controller.deleteItem);

api.patch('/:accountId/:movementId?', controller.off);

api.get('/:accountId/:movementId?', controller.get);

module.exports = api;
