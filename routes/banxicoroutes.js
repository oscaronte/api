'use strict';
const express = require('express');
const controller = require('../controllers/banxicocontroller');

var api = express.Router();

api.get('/dollar/:mxn?', controller.exchangeRate);

module.exports = api;
