'use strict';
const express = require('express');
const authservice = require('../services/Authentication');
const refreshservice = require('../services/RefreshAuth');
const controller = require('../controllers/client');

var api = express.Router();

api.use(authservice.validateToken);
api.use(refreshservice.refreshToken);

api.post('/', controller.create);

api.put('/', controller.off);

api.delete('/', controller.off);

api.patch('/', controller.off);

api.get('/:clientId?', controller.get);

module.exports = api;
