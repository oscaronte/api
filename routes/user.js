'use strict';
const express = require('express');
const controller = require('../controllers/user');
const authservice = require('../services/Authentication');

var api = express.Router();

api.post('/', controller.singup);

api.get('/', controller.off);

api.put('/',controller.off);

api.delete('/', controller.off);

api.patch('/', controller.off);

api.get('/activate/:token?', authservice.validateUniqueToken, controller.activate);

api.post('/login', controller.login);

module.exports = api;
