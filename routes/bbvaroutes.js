'use strict';
const express = require('express');
const controller = require('../controllers/bbvacontroller');

var api = express.Router();

api.get('/Vehicles', controller.vehicles);

api.get('/Merchants', controller.merchants);

api.post('/Simulation', controller.simulation);

module.exports = api;
