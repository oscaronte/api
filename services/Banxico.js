'use strict';
const requestjson = require('request-json');
const ExchangeRate = require('../model/ExchangeRate');
const utils = require('../modules/Utils');
const constants = require('../modules/Banxico');
const apiClient = requestjson.createClient(constants.exchangeURL);

apiClient.headers['Bmx-Token'] = constants.token;

function getExchangeRate(res, date, pesos) {
  apiClient.get(constants.exchangeURL+ '/' + date + '/' + date, function(err, response, body){
    if(err) utils.doResponse(res, 500, "Banxico/err", err);
    else {
      if(body && body.bmx) {
        let exchangeRate = body.bmx.series[0];
        exchangeRate.fecha = exchangeRate.datos[0].fecha;
        exchangeRate.dato = exchangeRate.datos[0].dato;
        if(exchangeRate) {
          ExchangeRate(exchangeRate).save(function(error, data){
            if(error) utils.doResponse(res, 500, "Internal Server Error");
            else {
              if(pesos) {
                convertPesos(res, data, pesos);
              } else {
                utils.doResponse(res, 200,"Banxico/live", data);
              }
            }
          });
        } else {
          utils.doResponse(res, 204, "Banxico/empty");
        }
      } else {
        utils.doResponse(res, 204, "Banxico/empty");
      }
    }
  });
}

function convertPesos(res, exchangeRate, pesos) {
  let data = {
    mxn: pesos,
    usd: '0',
    fecha: '',
    exchangeRate: ''
  }
  if(exchangeRate) {
    data.exchangeRate = exchangeRate.dato;
    data.fecha = exchangeRate.fecha;
    if(isNaN(pesos)) {
      utils.doResponse(res, 400, "Banxico/convert", "Debe ingresar una cifra correcta: <" + pesos + ">");
    } else {
      let mxn = parseFloat(pesos);
      let rate = parseFloat(exchangeRate.dato);
      let usd = String(mxn/rate);
      data.usd = usd;
      utils.doResponse(res, 200, "Banxico/convert", data);
    }
  } else {
    utils.doResponse(res, 204, 'Banxico/convert', data);
  }
}

module.exports = {getExchangeRate, convertPesos};
