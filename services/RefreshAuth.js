'use strict';
const jwt = require('./jwt');
const UserModel = require('../model/User');

exports.refreshToken = function(req, res, next) {
  if(req.method.toUpperCase()=='OPTIONS') {
    next();
  } else {
    let userId = req.user.sub;
    UserModel.findById({_id:userId}).populate({path:"client"}).exec(function(err, data){
      if(err) console.error(err);
      else {
        if(data) {
          let token = jwt.createToken(data);
          res.header("Refresh", token);
        }
      }
      next();
    });
  }
}
