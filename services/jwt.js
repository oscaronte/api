'use strict';
 const jwt = require('jwt-simple');
 const moment = require('moment');
 const utils = require('../modules/Utils');
 const key = 'Prac+itioner3aEdM3xico';

function createToken(user) {
  let client = user.client;
  let clientId = '';
  let email = '';
  let name = '';
  let customerId = '';
  let picLarge = '';
  let picMedium = '';
  let picThumb = '';
  if(client) {
    clientId = user.client._id;
    email = user.client.email;
    name = user.client.name.first + ' ' + user.client.name.last;
    customerId = user.client.login.customerId;
    picLarge = user.client.picture.large;
    picMedium = user.client.picture.medium;
    picThumb = user.client.picture.thumbnail;
  }
  let payload = {
    sub: user._id,
    custsub: clientId,
    name: name,
    email: email,
    customerId: customerId,
    picture: {
      large: picLarge,
      medium: picMedium,
      thumbnail: picThumb
    },
    role: user.role,
    uuid: user.uuid,
    iat: moment().unix(),
    exp: moment().add(5, 'minutes').unix()
  }

  return jwt.encode(payload, key);
}

function decodeToken(res, token) {
  var payload;

  try {
    payload = jwt.decode(token, key);
  } catch(ex) {
    console.error('Token no válido/expirado');
    utils.doResponse(res, 404, 'Token/invalid');
    payload = null;
  }

  return payload;
}

function createUniqueToken(info, minutes) {
  info.iat = moment().unix();
  info.exp = moment().add(minutes, 'minutes').unix();
  return jwt.encode(info, key);
}

 module.exports = {
   createToken, decodeToken, createUniqueToken
 }
