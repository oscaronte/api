'use strict';
const requestjson = require('request-json');
const utils = require('../modules/Utils');
const authMarket = require('../modules/ApiMarket');
const apiURL = 'https://apis.bbvabancomer.com/loans-auto-sbx/v1';
const epMechants = '/merchant';
const epVehicles = '/vehicles?brandId=0';
const epLoanSim = '/options-installment';
const marketClient = requestjson.createClient(apiURL);

function getMerchants(res, merchant) {
  authMarket.authorize(res, sendMerchants);
}

function getVehicles(res, brandId, year) {
  authMarket.authorize(res, sendVehicles);
}

function loanSimulation(res, loanPayload) {
  authMarket.authorize(res, sendLoanSim, loanPayload);
}

function sendVehicles(res, token) {
  marketClient.headers['Authorization'] = 'jwt ' + token;
  marketClient.get(apiURL+epVehicles, function(err, response, body) {
    if(err) utils.doResponse(res, 500, "Market/err", err);
    else {
      if(body && body.data) {
        utils.doResponse(res, 200, "Vehicles", body.data);
      } else {
        utils.doResponse(res, 204, "Market/empty");
      }
    }
  });
}

function sendMerchants(res, token) {
  marketClient.headers['Authorization'] = 'jwt ' + token;
  marketClient.get(apiURL+epMechants, function(err, response, body) {
    if(err) utils.doResponse(res, 500, "Market/err", err);
    else {
      if(body && body.data) {
        utils.doResponse(res, 200, "Merchants", body.data);
      } else {
        utils.doResponse(res, 204, "Market/empty");
      }
    }
  });
}

function sendLoanSim(res, token, payload) {
  marketClient.headers['Authorization'] = 'jwt ' + token;
  marketClient.post(apiURL+epLoanSim, payload, function(err, response, body) {
    if(err) utils.doResponse(res, 500, "Market/err", err);
    else {
      if(body) {
        if(body.result.code==200) {
          utils.doResponse(res, 200, "LoanSim", body.data);
        } else {
          utils.doResponse(res, body.result.detail, "LoanSim/err", body.result.info);
        }
      } else {
        utils.doResponse(res, 204, "Market/empty");
      }
    }
  });
}

module.exports = {
  getMerchants, getVehicles, loanSimulation
}
