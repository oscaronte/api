'use strict';
const jwt = require('./jwt');
const utils = require('../modules/Utils');

exports.validateToken = function(req, res, next) {
  if(req.method.toUpperCase()=='OPTIONS') {
    next();
  } else if(req.headers.authorization) {
    let token = req.headers.authorization.replace(/['"]+/g, '');
    let user = jwt.decodeToken(res, token);
    if(user) {
      req.user = user;
      next();
    }
  } else {
    utils.doResponse(res, 403, "No autorizado");
  }
}

exports.validateUniqueToken = function(req, res, next) {
  let token = null;
  
  if(req.params.token) {
    token = req.params.token;
  } else if(req.query.token) {
    token = req.query.token;
  } else if(req.body.token) {
    token = req.body.token;
  }
  if(token) {
    let info = jwt.decodeToken(res, token);
    if(info) {
      req.info = info;
      next();
    }
  } else {
    utils.doResponse(res, 404, "Token/empty");
  }
}
