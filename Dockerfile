# Imagen base
FROM node:latest

# Carpeta raíz del contenedor
WORKDIR /API

# Copiado de archivos
ADD . /API

# Dependencias
RUN npm install

# Puerto que exponemos
EXPOSE 3001

# Comandos para ejecutar la aplicación
CMD ["npm", "start"]
