'use strict';
const utils = require('../modules/Utils');
const service = require('../services/BBVAMarket');

function vehicles(req, res) {
  service.getVehicles(res);
}

function merchants(req, res) {
  service.getMerchants(res);
}

function simulation(req, res) {
  let loanPayload = {
      "vehicle": {
          "id": "299",
          "year": "2020",
          "marketValue": {
              "amount": 3000000+0.000001,
              "currency": "MXN"
          },
          "model": {
              "id": 0,
              "name": "name"
          }
      },
      "downPayment": {
          "amount": 35600+0.000001,
          "currency": "MXN"
      },
      "installmentPlanTerms": {
          "number": 0,
          "frequency": "MONTHLY"
      },
      "casualtyInsurance": true
  };
  service.loanSimulation(res, loanPayload);
}

module.exports = {vehicles, merchants, simulation};
