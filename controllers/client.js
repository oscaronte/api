'use strict';
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-pagination');
const requestjson = require('request-json');
const ClientModel = require('../model/Client');
const utils = require('../modules/Utils');

function get(req, res) {
  let user = req.user;
  let clientId = null;
  if(req.params.clientId) {
    clientId = req.params.clientId;
  }
  if(user.role=='CLIENT') {
    if(clientId==user.custsub) {
      getOne(res, clientId);
    } else {
      utils.doResponse(res, 401, "Unauthorized");
    }
  } else if(user.role=='ADMIN') {
    if(clientId) {
      getOne(res, clientId);
    } else {
      getAll(req, res);
    }
  } else {
    utils.doResponse(res, 406, "Not Acceptable");
  }
}

function create(req, res) {
  let user = req.user;

  if(user.role=='ADMIN') {
    let randomuser = requestjson.createClient('https://randomuser.me/api/');
    randomuser.get('', function(err, response, body) {
      if(err) utils.doResponse(res, 500, "No hay Random User");
      else {
        if(body && body.info.results!=0) {
          let client = body.results[0];
          client.login.customerId = client.login.username;
          ClientModel(client).save(function(err, data) {
            if(err) utils.doResponse(res, 500, "Internal Server Error");
            else utils.doResponse(res, 200, "OK");
          });
        } else {
          utils.doResponse(res, 204, "No Content");
        }
      }
    });
  } else {
    utils.doResponse(res, 403, "Forbidden");
  }
}

function off(req, res) {
  utils.doResponse(res, 405, "Method Not Allowed");
}

function getOne(res, clientId) {
  ClientModel.findById({_id:mongoose.Types.ObjectId(clientId)}, function(err, data) {
    if(err) utils.doResponse(res, 500, "Internal Server Error");
    else {
      if(data) utils.doResponse(res, 200, "Client", {client:data});
      else utils.doResponse(res, 204, "No Content");
    }
  });
}

function getAll(req, res) {
  let page = 1;
  if(req.query.page) {
    page = parseInt(req.query.page);
  }
  let items = 10;
  if(req.query.items) {
    items = parseInt(req.query.items);
  }
  ClientModel.find().sort('name.last name.first').paginate(page, items, function(err, data, count) {
    if(err) utils.doResponse(res, 500, 'Error');
    else utils.doResponse(res, 200, "Client", {clients:data, count:count, page:page});
  });
}

module.exports = {
  get, create, off
}
