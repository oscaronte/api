'use strict';
const moment = require('moment');
const utils = require('../modules/Utils');
const ExchangeRate = require('../model/ExchangeRate');
const service = require('../services/Banxico');

function exchangeRate(req, res) {
  let theDate;
  let time;
  let maxDate = moment().subtract(1, 'days');
  let pesos = req.params.mxn;

  if(req.query.date) {
    time = moment(req.query.date, 'DD/MM/YYYY');
    if(time<=maxDate) {
      theDate = time.format('YYYY-MM-DD');
    } else {
      return utils.doResponse(res, 400, "Invalid date", "La fecha no puede pasar de " + maxDate.format('DD/MM/YYYY'));
    }
  } else {
    time = maxDate;
    theDate = maxDate.format('YYYY-MM-DD');
  }
  ExchangeRate.findOne({"fecha":time.format('DD/MM/YYYY')}, function(err, data){
    if(err) utils.doResponse(res, 500, "Internal Server Error");
    else {
      if(data) {
        if(pesos) {
          service.convertPesos(res, data, pesos);
        } else {
          utils.doResponse(res, 200, "Banxico/cached", data);
        }
      } else {
        service.getExchangeRate(res, theDate, pesos);
      }
    }
  });
}

module.exports = {exchangeRate};
