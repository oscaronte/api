'use strict';
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-pagination');
const AccountModel = require('../model/Account');
const MovementModel = require('../model/Movement');
const utils = require('../modules/Utils');

function create(req, res) {
  let user = req.user;
  if(req.body.client) {
    let client = req.body.client;
    if(user.role=='ADMIN' || user.custsub==client) {
      AccountModel(req.body).save(function(err, data) {
        if(err) utils.doResponse(res, 500, "Internal Server Error");
        else utils.doResponse(res, 200, "OK");
      });
    } else {
      utils.doResponse(res, 401, "Unauthorized");
    }
  } else {
    utils.doResponse(res, 401, "Unauthorized");
  }
}

function get(req, res) {
  let user = req.user;
  if(user.role=='CLIENT') {
    if(req.params.accountId) {
      getAccount(res, req.params.accountId, user.custsub);
    } else {
      getClientAccounts(req, res, user.custsub);
    }
  } else if(user.role=='ADMIN') {
    if(req.params.accountId) {
      getAccount(res, req.params.accountId);
    } else {
      getAllAccounts(req, res);
    }
  } else {
    utils.doResponse(res, 401, "Unauthorized");
  }
}

function update(req, res) {
  let user = req.user;
  if(req.body.client && req.body._id) {
    let client = req.body.client;
    if(user.role=='ADMIN' || user.custsub==client) {
      let filter = {
        _id: mongoose.Types.ObjectId(req.body._id),
        client: mongoose.Types.ObjectId(client)
      }
      AccountModel.findOneAndUpdate(filter, req.body, function(err, data) {
        if(err) utils.doResponse(res, 500, "Internal Server Error");
        else utils.doResponse(res, 200, "OK");
      });
    } else {
      utils.doResponse(res, 401, "Unauthorized");
    }
  } else {
    utils.doResponse(res, 401, "Unauthorized");
  }
}

function deleteItem(req, res) {
  if(req.user.role=='ADMIN') {
    if(req.params.accountId) {
      AccountModel.deleteOne({_id:mongoose.Types.ObjectId(req.params.accountId)}, function(err, data) {
        if(err) utils.doResponse(res, 500, "Internal Server Error");
        else {
          if(data) {
            MovementModel.deleteMany({account:data._id}, function(err, data){
              if(err) utils.doResponse(res, 500, "Internal Server Error");
              else utils.doResponse(res, 200, "OK");
            });
          } else {
            utils.doResponse(res, 404, "Not Found");
          }
        }
      });
    } else {
      utils.doResponse(res, 400, "Bad request");
    }
  } else {
    utils.doResponse(res, 401, "Unauthorized");
  }
}

function off(req, res) {
  utils.doResponse(res, 405, "Method Not Allowed");
}

function getAccount(res, accountId, clientId) {
  let filter = {_id:accountId};
  if(clientId) {
    filter.client = mongoose.Types.ObjectId(clientId);
  }
  AccountModel.find(filter).populate({path:"client"}).exec(function(err, data) {
    if(err) utils.doResponse(res, 500, "Error");
    else {
      if(data && data.length!=0) {
        utils.doResponse(res, 200, "Account", {account:data[0]});
      } else {
        utils.doResponse(res, 204, "No Content");
      }
    }
  });
}

function getClientAccounts(req, res, clientId) {
  let filter = { client:mongoose.Types.ObjectId(clientId) };
  let page = 1;
  if(req.query.page) {
    page = parseInt(req.query.page);
  }
  let items = 10;
  if(req.query.items) {
    items = parseInt(req.query.items);
  }
  AccountModel.find(filter).sort('type number').paginate(page, items, function(err, data, count) {
    if(err) utils.doResponse(res, 500, "Internal Server Error");
    else utils.doResponse(res, 200, "Account", {accounts:data, count:count, page:page});
  });
}

function getAllAccounts(req, res) {
  let page = 1;
  if(req.query.page) {
    page = parseInt(req.query.page);
  }
  let items = 10;
  if(req.query.items) {
    items = parseInt(req.query.items);
  }
  AccountModel.find().populate({path:"client"}).sort('client.name.last client.name.first type number')
    .paginate(page, items, function(err, data, count) {
      if(err) utils.doResponse(res, 500, "Internal Server Error");
      else utils.doResponse(res, 200, "Account", {accounts:data, count:count, page:page});
    });
}

module.exports = {
  create, update, get, deleteItem, off
}
