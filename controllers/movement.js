'use strict';
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-pagination');
const MovementModel = require('../model/Movement');
const AccountModel = require('../model/Account');
const utils = require('../modules/Utils');

function create(req, res) {
  let user = req.user;
  let accountId = req.params.accountId;

  if(user.role=='CLIENT') {
    AccountModel.find({_id:mongoose.Types.ObjectId(accountId), client:user.custsub}, function(err, account) {
      if(err) utils.doResponse(res, 500, "Internal Server Error", err);
      else {
        if(account && account.length!=0) {
          MovementModel(req.body).save(function(err, data) {
            if(err) utils.doResponse(res, 500, "Internal Server Error", err);
            else utils.doResponse(res, 200, "Movement/create", {movement:data});
          });
        } else {
          utils.doResponse(res, 401, "Unauthorized");
        }
      }
    });
  } else {
    utils.doResponse(res, 401, "Unauthorized");
  }
}

function update(req, res) {
  let user = req.user;
  let accountId = req.params.accountId;
  let movementId = req.params.movementId;

  if(user.role=='CLIENT' && movementId) {
    AccountModel.find({_id:mongoose.Types.ObjectId(accountId), client:user.custsub}, function(err, account){
      if(err) utils.doResponse(res, 500, "Internal Server Error", err);
      else {
        if(account && account.length!=0) {
          MovementModel.findById({_id:mongoose.Types.ObjectId(movementId)}, function(err, data) {
            if(err) utils.doResponse(res, 500, "Internal Server Error", err);
            else utils.doResponse(res, 200, "Movement", {movement:data});
          });
        }
      }
    });
  } else {
    utils.doResponse(res, 401, "Unauthorized");
  }
}

function get(req, res) {
  let accountId = req.params.accountId;
  let movementId = req.params.movementId;
  let user = req.user;

  if(user.role=='CLIENT') {
    if(movementId) {
      getMovement(res, movementId, accountId, user.custsub);
    } else {
      getMovementsAccount(req, res, accountId, user.custsub);
    }
  } else if(user.role=='ADMIN') {
    if(movementId) {
      getMovement(res, movementId);
    } else {
      getMovementsAccount(req, res, accountId);
    }
  } else {
    utils.doResponse(res, 401, "Unauthorized");
  }
}

function deleteItem(req, res) {
  let user = req.user;
  let accountId = req.params.accountId;
  let movementId = req.params.movementId;

  if(user.role=='CLIENT' && movementId) {
    AccountModel.find({_id:mongoose.Types.ObjectId(accountId), client:user.custsub}, function(err, account){
      if(err) utils.doResponse(res, 500, "Internal Server Error", err);
      else {
        if(account && account.length!=0) {
          MovementModel.deleteOne({_id:mongoose.Types.ObjectId(movementId)}, function(err, data){
            if(err) utils.doResponse(res, 500, "Internal Server Error", err);
            else utils.doResponse(res, 200, "Movement/delete", {movement:data});
          });
        }
      }
    });
  } else {
    utils.doResponse(res, 401, "Unauthorized");
  }
}

function off(req, res) {
  utils.doResponse(res, 405, "Method Not Allowed");
}

function getMovement(res, movementId, accountId, clientId) {
  if(clientId) {
    AccountModel.find({_id:mongoose.Types.ObjectId(accountId), client:clientId}, function(err, account) {
      if(err) utils.doResponse(res, 500, "Internal Server Error", err);
      else {
        if(account && account.length!=0) {
          doGetMovement(res, movementId);
        } else {
          utils.doResponse(res, 401, "Unauthorized");
        }
      }
    });
  } else {
    doGetMovement(res,movementId);
  }
}

function doGetMovement(res, movementId) {
  MovementModel.find({_id:mongoose.Types.ObjectId(movementId)}).populate({path: "account"})
    .exec(function(err, data) {
      if(err) utils.doResponse(res, 500, "Internal Server Error", err);
      else {
        if(data) {
          utils.doResponse(res, 200, "Movement", {movement:data});
        } else {
          utils.doResponse(res, 404, "Not Found");
        }
      }
    });
}

function getMovementsAccount(req, res, accountId, clientId) {
  let page = 1;
  if(req.query.page) {
    page = parseInt(req.query.page);
  }
  let items = 10;
  if(req.query.items) {
    items = parseInt(req.query.items);
  }
  if(clientId) {
    AccountModel.find({_id:mongoose.Types.ObjectId(accountId), client:clientId}, function(err, account) {
      if(err) utils.doResponse(res, 500, "Internal Server Error", err);
      else {
        if(account && account.length!=0) {
          doGetMovementsAccount(res, accountId, page, items);
        } else {
          utils.doResponse(res, 401, "Unauthorized");
        }
      }
    });
  } else {
    doGetMovementsAccount(res, accountId, page, items);
  }
}

function doGetMovementsAccount(res, accountId, page, items) {
  MovementModel.find({account:mongoose.Types.ObjectId(accountId)}).populate({path:"account"})
    .paginate(page, items, function(err, data, count) {
      if(err) utils.doResponse(res, 500, "Internal Server Error", err);
      else {
        if(data && data.length!=0) {
          utils.doResponse(res, 200, "Movements", {movements:data, count:count, page:page});
        } else {
          utils.doResponse(res, 204, "Movements/empty");
        }
      }
    });
}

module.exports = {
  create, update, get, deleteItem, off
}
