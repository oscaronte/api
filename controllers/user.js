'use strict';
const mongoose = require('mongoose');
const bcrypt = require('bcrypt-node');
const {v4: uuidv4} = require('uuid');
const UserModel = require('../model/User');
const ClientModel = require('../model/Client');
const jwt = require('../services/jwt');
const emailservice = require('../services/EmailService');
const emailtemplates = require('../modules/EmailTemplates');
const utils = require('../modules/Utils');

function singup(req, res) {
  let params = req.body;
  if(params && params.userName && params.password && params.customerId) {
    let activate = req.query.activate || req.body.activate || emailtemplates.urlActivate;
    let redirect = req.query.redirect || req.body.redirect;
    validateClient(res, params, activate, redirect);
  } else {
    utils.doResponse(res, 400, "Bad Request");
  }
}

function off(req, res) {
  utils.doResponse(res, 405, "Method Not Allowed");
}

function activate(req, res) {
  let userId = req.info.userId;
  let url = req.info.redirect;
  if(userId) {
    activateUser(res, userId, url);
  } else {
    utils.doResponse(res, 400, "Bad Request");
  }
}

function login(req, res) {
  let params = req.body;
  if(params && params.userName && params.password) {
    doLogin(res, params);
  } else {
    utils.doResponse(res, 400, "Bad Request");
  }
}

function validateClient(res, params, activate, redirect) {
  ClientModel.findOne({"login.customerId":params.customerId.toLowerCase()}, "_id, email", function(err, client) {
    if(err) utils.doResponse(res, 500, "Internal Server Error");
    else {
      if(client)
        validateUser(res, params, activate, redirect, client._id, client.email);
      else
        utils.doResponse(res, 406, "Not Acceptable");
    }
  });
}

function validateUser(res, params, activate, redirect, clientId, clientEmail) {
  let filter = {
    "user":params.userName.toLowerCase()
  }
  UserModel.findOne(filter, "user status client", function(err, user) {
    if(err) utils.doResponse(res, 500, "Internal Server Error");
    else {
      if(user) {
        if(user.status=="0" && user.client.toString()==clientId) {
          bcrypt.hash(params.password, null, null, function(err, hash) {
            if(err) utils.doResponse(res, 500, "Internal Server Error");
            else {
              UserModel.findByIdAndUpdate(user._id, {password:hash}, function(err, data){
                if(err) utils.doResponse(res, 500, "Internal Server Error");
                else {
                  let body = emailtemplates.activate(activate, jwt.createUniqueToken({userId:user._id, redirect:redirect, uuid:uuidv4()}, 10));
                  emailservice(clientEmail,"🚨 Activa tu cuenta 🔒", body);
                  utils.doResponse(res, 200, "Revise su correo para activar la cuenta");
                }
              });
            }
          });
        } else {
          utils.doResponse(res, 406, "Not Acceptable");
        }
      } else {
        var newUser = {
          user: params.userName.toLowerCase(),
          status: "0",
          role: "CLIENT",
          client: clientId
        }
        bcrypt.hash(params.password, null, null, function(err, hash) {
          if(err) utils.doResponse(res, 500, "Internal Server Error");
          newUser.password = hash;
          UserModel(newUser).save(function(err, data) {
            if(err) utils.doResponse(res, 500, "Internal Server Error");
            else {
              let body = emailtemplates.activate(activate, jwt.createUniqueToken({userId:data._id, redirect:redirect, uuid:uuidv4()}, 10));
              emailservice(clientEmail,"🚨 Activa tu cuenta 🔒", body);
              utils.doResponse(res, 200, "Revise su correo para activar la cuenta");
            }
          });
        });
      }
    }
  });
}

function activateUser(res, userId, url) {
  let filter = {
    _id:mongoose.Types.ObjectId(userId),
    status: "0",
    role: "CLIENT"
  }
  UserModel.findOneAndUpdate(filter,{$set:{status:"1"}})
    .populate({path:"client"}).exec(function(err, userBefore){
      if(err) utils.doResponse(res, 500, "Internal Server Error");
      else {
        if(userBefore) {
          let clientName = userBefore.client.name.first + ' ' + userBefore.client.name.last;
          let body = emailtemplates.activatedOk(clientName);
          emailservice(userBefore.client.email, "🔓 Tu cuenta ha sido activada ✅", body);
          if(url) {
            res.redirect(url);
          } else {
            utils.doResponse(res, 200, "Cuenta activada");
          }
        } else {
          utils.doResponse(res, 404, "Not Found");
        }
      }
    });
}

function doLogin(res, params) {
  let filter = {
    user: params.userName,
    status: "1"
  }
  UserModel.findOne(filter).populate({path:"client"}).exec(function(err, user) {
    if(err) utils.doResponse(res, 500, "Internal Server Error");
    else {
      if(user) {
        bcrypt.compare(params.password, user.password, function(err, check) {
          if(err) utils.doResponse(res, 500, "Internal Server Error");
          else {
            if(check) {
              user.uuid = uuidv4();
              UserModel.updateOne({_id:user._id}, {$set: {lastin:Date.now(), uuid:user.uuid}}, function(err, data) {
                if(err) {
                  console.error("Error registrando inicio de sesion...");
                  console.error(err);
                }
              });
              let name;
              if(user.client) {
                name = user.client.name.first + ' ' + user.client.name.last;
              }
              utils.doResponse(res, 200, 'OK', {token:jwt.createToken(user), client:name});
            } else {
              utils.doResponse(res, 403, "Forbidden");
            }
          }
        });
      } else {
        utils.doResponse(res, 403, "Forbidden");
      }
    }
  });
}

module.exports = {
  singup, off, activate, login
}
