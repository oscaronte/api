'use strict';

exports.doResponse = function(res, status, message, data) {
  let output = {
    "result": {
      "code": status,
      "info": message
    }
  };
  if(data) {
    output.data = data;
  }
  res.status(status).json(output);
}
