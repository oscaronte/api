'use strict';
const requestjson = require('request-json');
const utils = require('../modules/Utils');
const authHeader = "Basic YXBwLm14Lm9zbWFkZW1vOm5rdmFHeSNnRG5jRGtRRCRUOXFwYSo3Y29QY2NPckNENWoxcXo0bHNncUFRSFlMZ0dnbVozI0B2b1NAZnBVQVA=";
const authURL = "https://connect.bbvabancomer.com";

const authClient = requestjson.createClient(authURL);
authClient.headers['Authorization'] = authHeader;

function authorize(res, callback, payload) {
  authClient.post(authURL+'/token?grant_type=client_credentials', {}, function(err, response, body){
    if(err) utils.doResponse(res, 500, "Market Error", err);
    else {
      if(body) {
        callback(res, body.access_token, payload);
      } else {
        utils.doResponse(res, 409, "Market/No Authorization");
      }
    }
  });
}

module.exports = {authorize}
