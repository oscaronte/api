"use strict";
const urlActivate = 'http://127.0.0.1:3001/api/Users/activate/';

module.exports =  {
  urlActivate: urlActivate,
  activate: function(url, token) {
    let body = '<html>';
    body += '<head><style>';

    body += 'div.incident-subject {';
    body += 'padding: 5px 10px 10px 10px;';
    body += 'font-size: 28px;';
    body += 'color: #FFFFFF;'
    body += 'background-color: #043263;'
    body += '}';

    body += 'div.incident-message {';
    body += 'padding: 20px 40px 20px 40px;';
    body += 'border: 1px solid #043263;';
    body += 'box-shadow: 0px 5px 10px #043263;';
    body += '}';

    body += 'div.incident-image {';
    body += 'width: 200px;';
    body += 'height-min: 50px;';
    body += 'color: #F4F4F4;';
    body += 'background-color: #666666;';
    body += 'overflow-wrap: break-word;';
    body += 'border: 1px solid #121212;';
    body += 'padding: 8px;';
    body += '}';

    body += 'table.actions td {';
    body += 'padding: 20px 40px 20px 40px;';
    body += 'margin: 20px;';
    body += 'background-color: #1464a5;';
    body += 'cursor: pointer;';
    body += 'box-shadow: 0px 5px 10px #043263;';
    body += '} ';

    body += 'table.actions a {';
    body += 'color: #FFFFFF;';
    body += 'font-size: 18px;';
    body += 'text-decoration: none;';
    body += '} ';

    body += 'table.actions td:hover {';
    body += 'background-color: #004481;';
    body += 'font-weight: bolder;';
    body += '}';
    body += '</style></head><body>';
    body += '<div class="incident-message">';
    body += 'Para activar su cuenta es necesario que dé click en el botón.';
    body += '</div>';
    body += '<br>';
    body += '<table class="actions" align="center"><tr>';
    body += '<td><a href="' + url + token + '" target="_blank">Activar mi cuenta</a>';
    body += '</td>';
    body += '</tr></table>';
    body += '<br><div class="incident-message">';
    body += 'Este enlace caduca en 10 minutos.';
    body += '</div>';
    body += '</body>';
    body += '</html>';
    return body;
  },

  activatedOk: function(clientName) {
    let body = '<html>';
    body += '<head><style>';

    body += 'div.incident-subject {';
    body += 'padding: 5px 10px 10px 10px;';
    body += 'font-size: 28px;';
    body += 'color: #FFFFFF;'
    body += 'background-color: #043263;'
    body += '}';

    body += 'div.incident-message {';
    body += 'padding: 20px 40px 20px 40px;';
    body += 'border: 1px solid #043263;';
    body += 'box-shadow: 0px 5px 10px #043263;';
    body += '}';

    body += 'div.incident-image {';
    body += 'width: 200px;';
    body += 'height-min: 50px;';
    body += 'color: #F4F4F4;';
    body += 'background-color: #666666;';
    body += 'overflow-wrap: break-word;';
    body += 'border: 1px solid #121212;';
    body += 'padding: 8px;';
    body += '}';

    body += 'table.actions td {';
    body += 'padding: 20px 40px 20px 40px;';
    body += 'margin: 20px;';
    body += 'background-color: #1464a5;';
    body += 'cursor: pointer;';
    body += 'box-shadow: 0px 5px 10px #043263;';
    body += '} ';

    body += 'table.actions a {';
    body += 'color: #FFFFFF;';
    body += 'font-size: 18px;';
    body += 'text-decoration: none;';
    body += '} ';

    body += 'table.actions td:hover {';
    body += 'background-color: #004481;';
    body += 'font-weight: bolder;';
    body += '}';
    body += '</style></head><body>';
    body += '<div class="incident-message">';
    body += 'Hola ' + clientName + ', <br><br>';
    body += '<div class="incident-subject">Tu cuenta ha sido activada.</div>'
    body += '<br>Recuerda: tu contraseña es la que definiste al momento de tu registro.';
    body += '</div>';
    body += '<br>';
    body += '</body>';
    body += '</html>';
    return body;
  }
}
